using Assets.Scripts.Level;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class UndoManager : MonoBehaviour
    {
        private static UndoManager _instance;
        public static UndoManager Instance { get => _instance; }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                transform.parent = null;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private GameObject _firstIngredientCopy;
        private GameObject _secondIngredientCopy;
        private GameObject movedIngredient;

        private InitData _firstDataCopy;
        private InitData _secondDataCopy;

        private GridLevel _level;

        public void RecordMove(Ingredient firstIngredient, Ingredient secondIngredient, GridLevel level)
        {
            _firstDataCopy = firstIngredient.GetData();
            _secondDataCopy = secondIngredient.GetData();
            movedIngredient = secondIngredient.gameObject;

            if(_firstIngredientCopy == null)
            {
                Destroy(_firstIngredientCopy);
            }

            if(_secondIngredientCopy == null)
            {
                Destroy(_secondIngredientCopy);
            }

            _firstIngredientCopy = Instantiate(firstIngredient.gameObject, level.transform);
            _secondIngredientCopy = Instantiate(secondIngredient.gameObject, level.transform);

            _firstIngredientCopy.SetActive(false);
            _secondIngredientCopy.SetActive(false);

            _level = level;
        }

        public void Undo()
        {
            if(_firstIngredientCopy == null || _secondIngredientCopy == null) return;

            Destroy(movedIngredient);

            _firstIngredientCopy.GetComponent<Ingredient>().Initailize(_firstDataCopy);
            _secondIngredientCopy.GetComponent<Ingredient>().Initailize(_secondDataCopy);

            _firstIngredientCopy.SetActive(true);
            _secondIngredientCopy.SetActive(true);

            _firstIngredientCopy = null;
            _secondIngredientCopy = null;
            movedIngredient = null;
        }
        
    }
}
