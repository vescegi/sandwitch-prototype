﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.Managers
{
    public class InputManager : MonoBehaviour
    {
        private static InputManager instance;
        public static InputManager Instance { get => instance; }


        private readonly Vector3[] AvailableDirections =
        {
            Vector3.up, Vector3.down, Vector3.left, Vector3.right
        };

        [SerializeField, Min(0f)] private float _minDistance = 10f;
        [SerializeField, Min(0f)] private float _animationTime = 1f;
        [SerializeField] private LayerMask _ingredientMask;

        private Vector2 _beginTouchPosition;

        private Ingredient _ingredientHit;


        private bool _disableInput = false;
        private bool _gameOver = false;

        private void Awake()
        {
            instance = this;
        }

        private void Update()
        {
            if (Input.touchCount <= 0 || _disableInput || _gameOver) return;

            HandleTouch();
        }

        private void HandleTouch()
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    BeginTouch(touch);
                    break;

                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    EndTouch(touch);
                    break;

            }
        }

        private void EndTouch(Touch touch)
        {
            if (_ingredientHit == null) return;

            Vector2 endTouchPosition = touch.position;

            if (Vector2.Distance(endTouchPosition, _beginTouchPosition) < _minDistance) return;

            Vector3 direction = GetClosestCrossDirection(endTouchPosition - _beginTouchPosition);

            direction.Set(direction.x, 0, direction.y);
            bool moved = _ingredientHit.Move(direction, _animationTime);

            _disableInput = true;

            Invoke(nameof(EnableInput), moved ? _animationTime : _animationTime * 2f);

        }

        private void EnableInput()
        {
            _disableInput = false;
        }

        public void GameOver()
        {
            _gameOver = true;
        }

        private void BeginTouch(Touch touch)
        {
            _beginTouchPosition = touch.position;

            Ray ray = Camera.main.ScreenPointToRay(touch.position);

            _ingredientHit = null;
            if (Physics.Raycast(ray, out RaycastHit hitInfo, 100f, _ingredientMask))
            {
                if (hitInfo.collider.TryGetComponent(out Ingredient ingredient))
                {
                    _ingredientHit = ingredient;
                }
            }
        }

        private Vector3 GetClosestCrossDirection(Vector3 direction)
        {
            Vector3 closest = Vector3.zero;
            float closestDistance = float.MaxValue;

            foreach (Vector3 crossDirection in AvailableDirections)
            {
                float distance = Vector3.Distance(direction, crossDirection);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closest = crossDirection;
                }
            }

            return closest;
        }
    }
}