using Assets.Scripts.Level;
using Assets.Scripts.Managers;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GridBrushBase;

public struct InitData
{
    public bool BreadOnTop;
    public bool BreadOnBottom;

    public float IngredientHeight;
    public int IngredientCount;

    public Vector2Int GridCoordinates;

    public InitData(bool breadOnTop, bool breadOnBottom, float ingredientHeight, int ingredientCount, Vector2Int gridCoordinates)
    {
        BreadOnTop = breadOnTop;
        BreadOnBottom = breadOnBottom;
        IngredientHeight = ingredientHeight;
        IngredientCount = ingredientCount;
        GridCoordinates = gridCoordinates;
    }
}

public class Ingredient : MonoBehaviour
{

    [SerializeField] private bool _isBread;

    private bool _breadOnTop;
    private bool _breadOnBottom;


    private float _ingredientHeight;
    private int _ingredientCount;


    private Vector2Int _gridCoordinates;

    private Ingredient _landedIngredient;
    private BoxCollider _collider;
    private GridLevel _gridLevel;

    public float TotalHeight { get => _ingredientHeight * _ingredientCount; }
    public Vector2Int GridCoordinates { get => _gridCoordinates; }

    private void Awake()
    {
        _gridLevel = transform.parent.GetComponent<GridLevel>();
        _collider = GetComponent<BoxCollider>();
        _ingredientHeight = _collider.size.y;
        _landedIngredient = null;
        _ingredientCount = 1;

        _breadOnBottom = _isBread;
    }

    private void Start()
    {
        _gridCoordinates = _gridLevel.InversePoint(transform.localPosition);
        _gridLevel.SetIngredient(_gridCoordinates.x, _gridCoordinates.y, this);
    }

    public bool Move(Vector3 direction, float animationTime)
    {
        Vector2Int newCoordinates = new Vector2Int(_gridCoordinates.x + (int)direction.x, _gridCoordinates.y - (int)direction.z);

        if (newCoordinates.x < 0 || newCoordinates.x >= _gridLevel.GridSize.x ||
            newCoordinates.y < 0 || newCoordinates.y >= _gridLevel.GridSize.y)
        {
            StartWrongMoveAnimation(direction, animationTime);
            return false;
        }

        _landedIngredient = _gridLevel.GetIngredient(newCoordinates.x, newCoordinates.y);

        if (_landedIngredient == null || !CanLand(_landedIngredient))
        {
            StartWrongMoveAnimation(direction, animationTime);
            return false;
        }

        UndoManager.Instance.RecordMove(this, _landedIngredient, _gridLevel);

        _gridLevel.SetIngredient(_gridCoordinates.x, _gridCoordinates.y, null);

        _landedIngredient._breadOnTop = _breadOnBottom;

        MoveAnimation(direction, animationTime, true);

        return true;
    }

    private void MoveAnimation(Vector3 direction, float animationTime, bool commit)
    {
        Vector3 rotationDirection = new Vector3(direction.z, 0, -direction.x);
        Vector3 rotation = (rotationDirection * 180);

        transform.DORotate(rotation, animationTime, RotateMode.WorldAxisAdd);

        float landedHeight = _landedIngredient != null ? _landedIngredient.TotalHeight : 0f;

        float heightOffset = landedHeight + (this.TotalHeight - _ingredientHeight);

        if (commit)
            transform.DOLocalJump(transform.localPosition + direction + Vector3.up * heightOffset, 1f, 1, animationTime)
                .OnComplete(AttachToIngredient);
        else
            transform.DOLocalJump(transform.localPosition + direction + Vector3.up * heightOffset, 1f, 1, animationTime);
    }

    private void StartWrongMoveAnimation(Vector3 direction, float animationTime)
    {
        StartCoroutine(WrongMoveAnimation(direction, animationTime));
    }

    private IEnumerator WrongMoveAnimation(Vector3 direction, float animationTime)
    {
        Vector3 rotationDirection = new Vector3(direction.z, 0, -direction.x);
        Vector3 oldPosition = transform.localPosition;
        Vector3 rotation = -(rotationDirection * 180);
        MoveAnimation(direction, animationTime, false);
        
        yield return new WaitForSeconds(animationTime);

        transform.DORotate(rotation, animationTime, RotateMode.WorldAxisAdd);
        transform.DOLocalJump(oldPosition, 1f, 1, animationTime);
    }

    private void AttachToIngredient()
    {
        // Double loop because the childrens are removed from the list of this transform's children

        List<Transform> allChildrens = new List<Transform>();

        foreach (Transform child in transform)
        {
            allChildrens.Add(child);
        }

        foreach (Transform child in allChildrens)
        {
            _landedIngredient.AttachModel(child);
        }
        _landedIngredient.RecalculateCollider();
        _landedIngredient.WinCondition();
        Destroy(gameObject);
    }

    private void WinCondition()
    {
        if (!_breadOnBottom || !_breadOnTop) return;

        InputManager.Instance.GameOver();

        if(_gridLevel.IngredientCount() == 1)
        {
            FindObjectOfType<LevelUI>().Win();
        }
        else
        {
            FindObjectOfType<LevelUI>().Lose();
        }
    }

    private void AttachModel(Transform model)
    {
        model.parent = transform;
        _ingredientCount++;
    }

    private void RecalculateCollider()
    {
        Vector3 size = _collider.size;
        Vector3 center = _collider.center;

        center.y = (_ingredientCount - 1) * _ingredientHeight * 0.5f;
        size.y = TotalHeight;

        _collider.size = size;
        _collider.center = center;
    }

    private bool CanLand(Ingredient other)
    {
        return !other._breadOnTop && !(_breadOnTop && !other._breadOnBottom);
    }

    public void Initailize(in InitData data)
    {
        this._breadOnTop = data.BreadOnTop;
        this._breadOnBottom = data.BreadOnBottom;

        this._ingredientHeight = data.IngredientHeight;
        this._ingredientCount = data.IngredientCount;

        this._gridCoordinates = data.GridCoordinates;
    }

    public InitData GetData()
    {
        return new InitData(
            this._breadOnTop,
            this._breadOnBottom,
            this._ingredientHeight,
            this._ingredientCount,
            this._gridCoordinates
            );
    }

    private void OnDestroy()
    {
        transform.DOKill();
    }
}

