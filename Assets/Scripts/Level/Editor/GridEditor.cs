﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Level
{
    [CustomEditor(typeof(GridLevel))]
    public class GridEditor : Editor
    {
        private GridLevel _grid;
        private void OnEnable()
        {
            _grid = target as GridLevel;
        }

        #region Inspector
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Snap childrens"))
            {
                SnapChildrens();
            }
        }

        public void SnapChildrens()
        {
            foreach (Transform children in _grid.transform)
            {
                Undo.RecordObject(children, "Updated position");
                EditorUtility.SetDirty(children);

                Vector3 newPosition = children.localPosition;
                newPosition.y = 0f;

                newPosition = SnapPointToClosest(newPosition);
                children.localPosition = newPosition;
            }
        }

        private Vector3 SnapPointToClosest(Vector3 childrenPosition)
        {
            Vector3 closestPoint = Vector3.zero;
            float closestDistance = float.MaxValue;

            for (int i = 0; i < _grid.GridSize.x; i++)
            {
                for (int j = 0; j < _grid.GridSize.y; j++)
                {
                    Vector3 point = _grid.GetLocalPosition(i, j);
                    float distance = Vector3.Distance(point, childrenPosition);

                    if(distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestPoint = point;
                    }
                }
            }

            return closestPoint;
        }
        #endregion
    }
}