﻿using System.Collections;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.Scripts.Level
{
    public class GridLevel : MonoBehaviour
    {
        [SerializeField] private Vector2Int _gridSize = new Vector2Int(4, 4);
        [SerializeField] private Vector2 _cellSize = Vector2.one;

        public Vector2 Dimentions { get => new Vector2(CellSize.x * GridSize.x, CellSize.y * GridSize.y); }

        public Vector2Int GridSize { get => _gridSize; set => _gridSize = value; }
        public Vector2 CellSize { get => _cellSize; set => _cellSize = value; }

        private Ingredient[,] _ingredientGrid;

        private void Awake()
        {
            _ingredientGrid = new Ingredient[GridSize.x, GridSize.y];
        }

        public Vector3 GetLocalPosition(int x, int y)
        {
            Vector2 dimentions = Dimentions;
            // Debug.Log(dimentions);
            return new Vector3(
                _cellSize.x * x + _cellSize.x * 0.5f - dimentions.x * 0.5f,
                0,
                _cellSize.y * y + _cellSize.y * 0.5f - dimentions.y * 0.5f
                );
        }

        public Vector2Int InversePoint(Vector3 position)
        {
            Vector3 offsetPoint = OffsetPoint(position);
            return new Vector2Int((int)(GridSize.x * 0.5f + offsetPoint.x), (int)(GridSize.y * 0.5f - offsetPoint.z));
        }

        private Vector3 OffsetPoint(Vector3 position)
        {
            return new Vector3(position.x - CellSize.x * 0.5f, 0, position.z + CellSize.y * 0.5f);
        }

        public Ingredient GetIngredient(int x, int y)
        {
            return _ingredientGrid[x, y];
        }

        public void SetIngredient(int x, int y, Ingredient ingredient)
        {
            _ingredientGrid[x, y] = ingredient;
        }

        public int IngredientCount()
        {
            int count = 0;

            for (int i = 0; i < GridSize.x; i++)
            {
                for (int j = 0; j < GridSize.y; j++)
                {
                    if (_ingredientGrid[i, j] != null)
                        count++;
                }
            }

            return count;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Handles.color = Color.white;
            GUIStyle textStyle = new GUIStyle();
            textStyle.normal.textColor = Color.green;


            Vector3 cellSize = new Vector3(CellSize.x, 0, CellSize.y);

            for (int i = 0; i < GridSize.x; i++)
            {
                for (int j = 0; j < GridSize.y; j++)
                {
                    Vector3 localCenter = GetLocalPosition(i, j);
                    Vector3 globalCenter = transform.TransformPoint(localCenter);

                    Vector3 localOffsetPoint = OffsetPoint(localCenter);
                    Vector3 globalOffsetPoint = transform.TransformPoint(localOffsetPoint);


                    Handles.DrawWireCube(globalCenter, cellSize);

                    Gizmos.DrawLine(globalCenter, globalOffsetPoint);


                    Handles.Label(globalOffsetPoint, InversePoint(localCenter).ToString(), textStyle);
                }
            }

        }
#endif
    }
}