using Assets.Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelUI : MonoBehaviour
{
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private GameObject _losePanel;
    [SerializeField] private GameObject _buttonsPanel;

    public void Undo()
    {
        UndoManager.Instance.Undo();
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
        SceneManager.LoadScene("LevelSelection");
    }

    public void Win()
    {
        _winPanel.SetActive(true);
        _buttonsPanel.SetActive(false);
    }

    public void Lose()
    {
        _losePanel.SetActive(true);
        _buttonsPanel.SetActive(false);
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
